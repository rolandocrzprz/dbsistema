/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Vista.jinEmpleados;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Empleados;
import modelo.dbEmpleados;

/**
 *
 * @author Rolando Cruz
 */
public class ControladorEmp implements ActionListener {

    private jinEmpleados vistaEmp;
    private dbEmpleados dbEmp;
    private boolean esActualizar;
    private int idEmpleado;

    public ControladorEmp(jinEmpleados vistaEmp, dbEmpleados dbEmp) {
        this.vistaEmp = vistaEmp;
        this.dbEmp = dbEmp;

        // Son los campos que escucha la vista de empleados....
        vistaEmp.btnNuevo.addActionListener(this);
        vistaEmp.btnGuardar.addActionListener(this);
        vistaEmp.btnBuscar.addActionListener(this);
        vistaEmp.btnDeshabilitar.addActionListener(this);
        vistaEmp.btnLimpiar.addActionListener(this);
        vistaEmp.btnCancelar.addActionListener(this);
        vistaEmp.btnCerrar.addActionListener(this);
        vistaEmp.btnHabilitar.addActionListener(this);

    }

    // Funcion para limpiar los campos...
    private void limpiar() {
        vistaEmp.txtCodigoEmp.setText("");
        vistaEmp.txtNombreEmp.setText("");
        vistaEmp.txtApellidoP.setText("");
        vistaEmp.txtApellidoM.setText("");
        vistaEmp.txtRolEmp.setText("");
        vistaEmp.jdFechaNac.setDate(null);
    }

    // Funcion para deshabilitar campos...
    private void deshabilitar() {
        // Campos de texto y fecha
        vistaEmp.txtCodigoEmp.setEnabled(false);
        vistaEmp.txtNombreEmp.setEnabled(false);
        vistaEmp.txtApellidoP.setEnabled(false);
        vistaEmp.txtApellidoM.setEnabled(false);
        vistaEmp.txtRolEmp.setEnabled(false);
        vistaEmp.jdFechaNac.setEnabled(false);

        // Botones
        vistaEmp.btnBuscar.setEnabled(false);
        vistaEmp.btnGuardar.setEnabled(false);
        vistaEmp.btnDeshabilitar.setEnabled(false);
        vistaEmp.btnHabilitar.setEnabled(false);

        // Tabla
        vistaEmp.tblEmpleados.setEnabled(false);
        vistaEmp.tblEmpleados.getTableHeader().setReorderingAllowed(false);
        vistaEmp.tblEmpleados.getTableHeader().setResizingAllowed(false);
    }

    // Funcion para habilitar campos...
    private void habilitar() {
        // Campos de texto y fecha
        vistaEmp.txtCodigoEmp.setEnabled(true);
        vistaEmp.txtNombreEmp.setEnabled(true);
        vistaEmp.txtApellidoP.setEnabled(true);
        vistaEmp.txtApellidoM.setEnabled(true);
        vistaEmp.txtRolEmp.setEnabled(true);
        vistaEmp.jdFechaNac.setEnabled(true);

        // Botones
        vistaEmp.btnBuscar.setEnabled(true);
        vistaEmp.btnGuardar.setEnabled(true);
        vistaEmp.btnDeshabilitar.setEnabled(true);
        vistaEmp.btnHabilitar.setEnabled(true);
    }

    private boolean validar() {
        boolean exito = true;

        if (vistaEmp.txtCodigoEmp.getText().isEmpty()
                || vistaEmp.txtNombreEmp.getText().isEmpty()
                || vistaEmp.txtApellidoP.getText().isEmpty()
                || vistaEmp.txtApellidoM.getText().isEmpty()
                || vistaEmp.txtRolEmp.getText().isEmpty()
                || vistaEmp.jdFechaNac.getDate() == null) {
            exito = false;
        }

        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaEmp.btnNuevo) {
            this.habilitar();
            this.esActualizar = false;
            vistaEmp.txtCodigoEmp.requestFocusInWindow();
        }
        if (e.getSource() == vistaEmp.btnGuardar) {
            if (this.validar()) {
                Empleados emp = new Empleados();
                emp.setCodigoEmp(vistaEmp.txtCodigoEmp.getText());
                emp.setNombre(vistaEmp.txtNombreEmp.getText());
                emp.setApellidoP(vistaEmp.txtApellidoP.getText());
                emp.setApellidoM(vistaEmp.txtApellidoM.getText());
                emp.setRolEmpleado(vistaEmp.txtRolEmp.getText());
                emp.setStatus(0);
                emp.setFechaNac(this.convertirAnioMesDia(vistaEmp.jdFechaNac.getDate()));

                try {
                    if (this.esActualizar == false) {
                        // Verifica que no exista el codigo en la BD antes de insertar un nuevo emp...
                        if (dbEmp.siExiste(emp.getCodigoEmp())) {
                            JOptionPane.showMessageDialog(vistaEmp, "El código ya existe. Por favor, use un código diferente.");
                            vistaEmp.txtCodigoEmp.requestFocusInWindow();
                        } else {
                            dbEmp.insertar(emp);
                            JOptionPane.showMessageDialog(vistaEmp, "Se agregó con éxito");
                            this.limpiar();
                            this.deshabilitar();
                            this.cargarTabla(dbEmp.lista());
                        }
                    } else {
                        // Actualizar datos de empleado existente..
                        emp.setIdEmpleado(idEmpleado);
                        dbEmp.actualizar(emp);
                        JOptionPane.showMessageDialog(vistaEmp, "Se actualizó con éxito");
                        this.limpiar();
                        this.deshabilitar();
                        this.cargarTabla(dbEmp.lista());
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vistaEmp, "Surgió un error: " + ex.getMessage());
                }

            } else {
                JOptionPane.showMessageDialog(vistaEmp, "Faltaron datos por capturar");
            }

        }
        if (e.getSource() == vistaEmp.btnBuscar) {
            Empleados emp = new Empleados();
            if (vistaEmp.txtCodigoEmp.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vistaEmp, "Falto capturar el codigo");
            } else {
                try {
                    emp = (Empleados) dbEmp.buscar(vistaEmp.txtCodigoEmp.getText());
                    if (emp.getIdEmpleado() != 0) {
                        idEmpleado = emp.getIdEmpleado();
                        vistaEmp.txtNombreEmp.setText(emp.getNombre());
                        vistaEmp.txtApellidoP.setText(emp.getApellidoP());
                        vistaEmp.txtApellidoM.setText(emp.getApellidoM());
                        vistaEmp.txtRolEmp.setText(emp.getRolEmpleado());
                        convertirStringDate(emp.getFechaNac());
                        this.esActualizar = true;

                    } else {
                        JOptionPane.showMessageDialog(vistaEmp, "no se encontro");
                    }
                } catch (Exception i) {
                    JOptionPane.showMessageDialog(vistaEmp, "Surgio un error " + i.getMessage());
                }
            }
        }
        if (e.getSource() == vistaEmp.btnHabilitar) {
            if (vistaEmp.txtCodigoEmp.getText().equals("")) {
                JOptionPane.showMessageDialog(vistaEmp, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vistaEmp, "Desea habilitar al empleado? ", "Empleado",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Empleados emp = new Empleados();
                    emp.setCodigoEmp(vistaEmp.txtCodigoEmp.getText());
                    try {
                        dbEmp.habilitar(emp);
                        JOptionPane.showMessageDialog(vistaEmp, "El empleado fue habilitado ");
                        this.limpiar();
                        this.cargarTabla(dbEmp.lista());
                        vistaEmp.txtCodigoEmp.requestFocusInWindow();
                        // ACTUALIZAR LA TABLA

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vistaEmp, "Surgio un error:  " + i.getMessage());
                    }
                }
            }
        }
        if (e.getSource() == vistaEmp.btnDeshabilitar) {
            if (vistaEmp.txtCodigoEmp.getText().equals("")) {
                JOptionPane.showMessageDialog(vistaEmp, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vistaEmp, "Desea deshabilitar el empleado? ", "Empleados",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Empleados emp = new Empleados();
                    emp.setCodigoEmp(vistaEmp.txtCodigoEmp.getText());
                    try {
                        dbEmp.deshabilitar(emp);
                        JOptionPane.showMessageDialog(vistaEmp, "El Empleado fue deshabilitado ");
                        this.limpiar();
                        this.cargarTabla(dbEmp.lista());
                        vistaEmp.txtCodigoEmp.requestFocusInWindow();
                        // ACTUALIZAR LA TABLA

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vistaEmp, "Surgio un error:  " + i.getMessage());
                    }
                }
            }
        }
        if (e.getSource() == vistaEmp.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (e.getSource() == vistaEmp.btnLimpiar) {
            this.limpiar();
        }
        if (e.getSource() == vistaEmp.btnCerrar) {
            int res = JOptionPane.showConfirmDialog(vistaEmp, "Deseas cerrar el sistema", "Empleado", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (res == JOptionPane.YES_OPTION) {
                vistaEmp.dispose();
            }
        }
    }

    public void iniciarVista() {
        vistaEmp.setTitle("Empleado");
        vistaEmp.setVisible(true);
        vistaEmp.resize(1046, 728);
        vistaEmp.setLocale(null);
        this.deshabilitar();
        try {
            this.cargarTabla(dbEmp.lista());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vistaEmp, "Surgio un error al cargar datos: " + ex);
        }
    }

    public void cargarTabla(ArrayList<Empleados> arr) {
        String campos[] = {"IdEmpleado", "Codigo", "Nombre", "apellidoP", "apellidoM", "Rol", "FechaNac"};
        String[][] datos = new String[arr.size()][7];
        int renglon = 0;
        for (Empleados registro : arr) {
            datos[renglon][0] = String.valueOf(registro.getIdEmpleado());
            datos[renglon][1] = registro.getCodigoEmp();
            datos[renglon][2] = registro.getNombre();
            datos[renglon][3] = registro.getApellidoM();
            datos[renglon][4] = registro.getApellidoP();
            datos[renglon][5] = registro.getRolEmpleado();
            datos[renglon][6] = registro.getFechaNac();
            renglon++;
        }

        DefaultTableModel tbl = new DefaultTableModel(datos, campos);
        vistaEmp.tblEmpleados.setModel(tbl);

    }

    public String convertirAnioMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStringDate(String fecha) {
        try {
            // Convertir la cadena de texto a un objeto Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vistaEmp.jdFechaNac.setDate(date);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
