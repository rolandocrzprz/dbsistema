/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import Vista.jinProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.sql.Date;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

// Manejo de fechas
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rolando Cruz
 */
public class Controlador implements ActionListener {

    private jinProductos vista;
    private dbProducto db;
    private boolean esActualizar;
    private int idProducto = 0;

    public Controlador(jinProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;

        // DEFINIMOS LOS CAMPOS QUE VA A ESCUCHAR DE LA VISTA....
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        
    }

    // FUNCIONES DE AYUDA
    private void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jdFecha.setDate(null);
    }

    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar el sistema ", "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
    }

    public void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);

        vista.btnBuscar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);

        vista.jdFecha.setEnabled(true);
    }

    public void deshabilitar() {
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);

        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
        vista.btnHabilitar.setEnabled(false);

        vista.jdFecha.setEnabled(false);

        
        vista.tblProductos.setEnabled(false);
        vista.tblProductos.getTableHeader().setReorderingAllowed(false);
        vista.tblProductos.getTableHeader().setResizingAllowed(false);


    }

    public boolean validar() {
        boolean exito = true;

        if (vista.txtCodigo.getText().equals("")
                || vista.txtNombre.getText().equals("")
                || vista.jdFecha.getDate() == null
                || vista.txtPrecio.getText().equals("")) {
            exito = false;
        }

        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.btnNuevo) {
            this.habilitar();
            this.esActualizar = false;
            vista.txtCodigo.requestFocusInWindow();
        }

        if (e.getSource() == vista.btnGuardar) {
            // Validar los datos de entrada
            if (this.validar()) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAnioMesDia(vista.jdFecha.getDate()));

                try {
                    if (this.esActualizar == false) {
                        // Verificar si el código ya existe
                        if (db.siExiste(pro.getCodigo())) {
                            JOptionPane.showMessageDialog(vista, "El código ya existe. Por favor, use un código diferente.");
                            vista.txtCodigo.requestFocusInWindow();
                        } else {
                            // Agregar nuevo producto
                            db.insertar(pro);
                            JOptionPane.showMessageDialog(vista, "Se agregó con éxito");
                            this.limpiar();
                            this.deshabilitar();
                            this.cargarTabla(db.lista());
                        }
                    } else {
                        // Actualizar producto existente
                        pro.setIdproductos(idProducto);
                        db.actualizar(pro);
                        JOptionPane.showMessageDialog(vista, "Se actualizó el producto con éxito");
                        this.cargarTabla(db.lista());
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió un error: " + ex.getMessage());
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
            }
        }

        if (e.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");

            } else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    if (pro.getIdproductos() != 0) {
                        idProducto = pro.getIdproductos();
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStringDate(pro.getFecha());
                        this.esActualizar = true;
                        vista.btnDeshabilitar.setEnabled(true);
                        vista.btnGuardar.setEnabled(true);
                    } else {
                        JOptionPane.showMessageDialog(vista, "no se encontro");
                    }

                } catch (Exception i) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error " + i.getMessage());
                }
            }
        }

        if (e.getSource() == vista.btnDeshabilitar) {

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vista, "Desea deshabilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    try {
                        db.deshabilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado ");
                        this.limpiar();
                        this.cargarTabla(db.lista());
                        vista.txtCodigo.requestFocusInWindow();
                        // ACTUALIZAR LA TABLA

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                    }
                }
            }

        }

        if (e.getSource() == vista.btnHabilitar) {

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vista, "Desea habilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    try {
                        db.habilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto fue habilitado ");
                        this.limpiar();
                        this.cargarTabla(db.lista());
                        vista.txtCodigo.requestFocusInWindow();
                        // ACTUALIZAR LA TABLA

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                    }
                }
            }

        }

        if (e.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        if (e.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (e.getSource() == vista.btnCerrar) {
            int res = JOptionPane.showConfirmDialog(vista, "Deseas cerrar el sistema", "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (res == JOptionPane.YES_OPTION) {
                vista.dispose();
            }
        }

    }

    public void cargarTabla(ArrayList<Productos> arr) {
        String campos[] = {"idProductos", "Codigo", "Nombre", "Precio", "Fecha"};
        String[][] datos = new String[arr.size()][5];
        int renglon = 0;

        for (Productos registro : arr) {
            datos[renglon][0] = String.valueOf(registro.getIdproductos());
            datos[renglon][1] = registro.getCodigo();
            datos[renglon][2] = registro.getNombre();
            datos[renglon][3] = String.valueOf(registro.getPrecio());
            datos[renglon][4] = registro.getFecha();
            renglon++;
        }
        DefaultTableModel tbl = new DefaultTableModel(datos, campos);
        vista.tblProductos.setModel(tbl);
    }

    public void iniciarVista() {
        vista.setTitle("Producto");
        vista.setVisible(true);
        vista.resize(1046, 728);
        vista.setLocale(null);
        this.deshabilitar();
        try {
            this.cargarTabla(db.lista());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista, "Surgio un error al cargar datos: " + ex);
        }
    }

    public String convertirAnioMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStringDate(String fecha) {
        try {
            // Convertir la cadena de texto a un objeto Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.jdFecha.setDate(date);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
