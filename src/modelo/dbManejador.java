/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.*;

/**
 *
 * @author Rolando Cruz
 */
public abstract class dbManejador {

    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario, contraseña, baseDatos, drive, url;

    public dbManejador() {
        this.baseDatos = "sistemas";
        this.drive = "com.mysql.cj.jdbc.Driver";
        this.url = "jdbc:mysql://13.59.243.197:3306/" + this.baseDatos;
        this.usuario = "rolando";
        this.contraseña = "123";
        this.EsDrive();

    }

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String contraseña, String baseDatos, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.baseDatos = baseDatos;
        this.drive = drive;
        this.url = url;
    }

    // METODO PARA BUSCAR EL DRIVE
    public boolean EsDrive() {
        boolean exito = false;

        try {

            Class.forName(drive);
            exito = true;

        } catch (ClassNotFoundException e) {

            System.out.println("Surgio un error: " + e.getMessage());
            System.exit(-1);
        }

        return exito;

    }

    // METODO CONECTAR
    public boolean conectar() {
        boolean exito = false;
        try {

            this.setConexion(DriverManager.getConnection(url, usuario, this.contraseña));
            exito = true;

        } catch (SQLException e) {

            System.out.println("Surgio un error: " + e.getMessage());
            System.exit(-1);
        }

        return exito;

    }

    // METODO DE DESCONECTAR
    public void desconectar() {

        try {
            // Validar que la conexion esta abierta
            if (!this.conexion.isClosed()) {
                this.conexion.close();
            }

        } catch (SQLException e) {
            System.out.println("Surgio un error al cerrar la conexion: " + e.getMessage());
        }

    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
