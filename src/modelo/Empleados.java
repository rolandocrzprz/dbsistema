/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rolando Cruz
 */
public class Empleados {
    
    private int idEmpleado;
    private String codigoEmp;
    private String nombre;
    private String apellidoP;
    private String apellidoM;
    private String fechaNac;
    private String rolEmpleado;
    private int status; // 0 = habilitado, 1 = no habilitado

    
    public Empleados() {
        this.idEmpleado = 0;
        this.codigoEmp = "";
        this.nombre = "";
        this.apellidoP = "";
        this.apellidoM = "";
        this.fechaNac = "";
        this.rolEmpleado = "";
        this.status = 0;
    }

    public Empleados(int idEmpleado, String codigoEmp, String nombre, String apellidoP, String apellidoM, String fechaNac, String rolEmpleado, int status) {
        this.idEmpleado = idEmpleado;
        this.codigoEmp = codigoEmp;
        this.nombre = nombre;
        this.apellidoP = apellidoP;
        this.apellidoM = apellidoM;
        this.fechaNac = fechaNac;
        this.rolEmpleado = rolEmpleado;
        this.status = status;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getCodigoEmp() {
        return codigoEmp;
    }

    public void setCodigoEmp(String codigoEmp) {
        this.codigoEmp = codigoEmp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getRolEmpleado() {
        return rolEmpleado;
    }

    public void setRolEmpleado(String rolEmpleado) {
        this.rolEmpleado = rolEmpleado;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    
    
    
    
}
