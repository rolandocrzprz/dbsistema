/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rolando Cruz
 */
public class dbEmpleados extends dbManejador implements Persistencia {

    public dbEmpleados() {
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Empleados emp = (Empleados) object;
        String consulta = "INSERT INTO empleados (codigoEmp, nombre, apellidoP, apellidoM, fechaNac, rol, status) VALUES (?, ?, ?, ?, ?, ?, 0)";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, emp.getCodigoEmp());
            this.sqlConsulta.setString(2, emp.getNombre());
            this.sqlConsulta.setString(3, emp.getApellidoP());
            this.sqlConsulta.setString(4, emp.getApellidoM());
            this.sqlConsulta.setString(5, emp.getFechaNac());
            this.sqlConsulta.setString(6, emp.getRolEmpleado());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Object object) throws Exception {
        Empleados emp = (Empleados) object;
        String consulta = "UPDATE empleados SET nombre = ?, apellidoP = ?, apellidoM = ?, fechaNac = ?, rol = ?, status = ? WHERE codigoEmp = ?";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, emp.getNombre());
            this.sqlConsulta.setString(2, emp.getApellidoP());
            this.sqlConsulta.setString(3, emp.getApellidoM());
            this.sqlConsulta.setString(4, emp.getFechaNac());
            this.sqlConsulta.setString(5, emp.getRolEmpleado());
            this.sqlConsulta.setInt(6, emp.getStatus());
            this.sqlConsulta.setString(7, emp.getCodigoEmp());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Empleados emp = (Empleados) object;
        String consulta = "UPDATE empleados SET status = 0 WHERE codigoEmp = ? AND status = 1";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, emp.getCodigoEmp());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Empleados emp = (Empleados) object;
        String consulta = "UPDATE empleados SET status = 1 WHERE codigoEmp = ? AND status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, emp.getCodigoEmp());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        boolean existe = false;
        String consulta = "SELECT 1 FROM empleados WHERE codigoEmp = ?";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                existe = true;
            }
            this.desconectar();
        }

        return existe;
    }

    @Override
    public ArrayList<Empleados> lista() throws Exception {
        ArrayList<Empleados> listaEmpleados = new ArrayList<>();
        String consulta = "SELECT * FROM empleados WHERE status = 0 ORDER BY codigoEmp";

        try {
            if (this.conectar()) {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.registros = this.sqlConsulta.executeQuery();

                while (this.registros.next()) {
                    Empleados emp = new Empleados();
                    emp.setIdEmpleado(this.registros.getInt("idEmpleado"));
                    emp.setCodigoEmp(this.registros.getString("codigoEmp"));
                    emp.setNombre(this.registros.getString("nombre"));
                    emp.setApellidoP(this.registros.getString("apellidoP"));
                    emp.setApellidoM(this.registros.getString("apellidoM"));
                    emp.setFechaNac(this.registros.getString("fechaNac"));
                    emp.setRolEmpleado(this.registros.getString("rol"));
                    emp.setStatus(this.registros.getInt("status"));

                    listaEmpleados.add(emp);
                }

                this.desconectar();
            }
        } catch (SQLException e) {
            System.out.println("Error al encontrar registros: " + e.getMessage());
            throw e;
        }

        return listaEmpleados;
    }

    @Override
    public Empleados buscar(String codigo) throws Exception {
        Empleados emp = new Empleados();
        String consulta = "SELECT * FROM empleados WHERE codigoEmp = ? AND status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            registros = this.sqlConsulta.executeQuery();

            if (this.registros.next()) {
                emp.setIdEmpleado(this.registros.getInt("idEmpleado"));
                emp.setCodigoEmp(this.registros.getString("codigoEmp"));
                emp.setNombre(this.registros.getString("nombre"));
                emp.setApellidoP(this.registros.getString("apellidoP"));
                emp.setApellidoM(this.registros.getString("apellidoM"));
                emp.setFechaNac(this.registros.getString("fechaNac"));
                emp.setRolEmpleado(this.registros.getString("rol"));
                emp.setStatus(this.registros.getInt("status"));
                this.desconectar();
            }
        }

        return emp;
    }
}
