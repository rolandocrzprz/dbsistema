/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rolando Cruz
 */
public class Productos {

    private int idproductos;
    private String codigo;
    private String nombre;
    private float precio;
    private String fecha;
    private int status; // 0 = habilitado, 1 = no habilitado

    public Productos() {
        this.idproductos = 0;
        this.codigo = "";
        this.nombre = "";
        this.precio = 0;
        this.fecha = "";
        this.status = 0;
    }

    public Productos(int idproductos, String codigo, String nombre, float precio, String fecha, int status) {
        this.idproductos = idproductos;
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.fecha = fecha;
        this.status = status;
    }

    public int getIdproductos() {
        return idproductos;
    }

    public void setIdproductos(int idproductos) {
        this.idproductos = idproductos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
