/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rolando Cruz
 */
public class dbProducto extends dbManejador implements Persistencia {

    public dbProducto() {
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;

        String consulta = "";
        consulta = "INSERT INTO productos (codigo, nombre, precio, fecha, status) VALUES (?, ?, ?, ?, 0)";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.setString(2, pro.getNombre());
            this.sqlConsulta.setFloat(3, pro.getPrecio());
            this.sqlConsulta.setString(4, pro.getFecha());
//            this.sqlConsulta.setInt(5, pro.getStatus());

            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = (Productos) object;

        String consulta = "UPDATE productos SET nombre = ?, precio = ?, fecha = ?, status = ? WHERE codigo = ?";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getNombre());
            this.sqlConsulta.setFloat(2, pro.getPrecio());
            this.sqlConsulta.setString(3, pro.getFecha());
            this.sqlConsulta.setInt(4, pro.getStatus());
            this.sqlConsulta.setString(5, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = (Productos) object;
        String consulta = "UPDATE productos set status = 0 WHERE codigo = ? and status = 1";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = (Productos) object;
        String consulta = "UPDATE productos set status = 1 WHERE codigo = ? and status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        boolean existe = false;
        String consulta = "SELECT 1 FROM productos WHERE codigo = ?";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                existe = true;
            }
            this.desconectar();
        }

        return existe;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();

        String consulta = "SELECT * FROM productos WHERE status = 0 ORDER BY codigo";

        try {

            if (this.conectar()) {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.registros = this.sqlConsulta.executeQuery();

                while (this.registros.next()) {
                    Productos pro = new Productos();
                    pro.setIdproductos(this.registros.getInt("idproductos"));
                    pro.setCodigo(this.registros.getString("codigo"));
                    pro.setNombre(this.registros.getString("nombre"));
                    pro.setPrecio(this.registros.getFloat("precio"));
                    pro.setFecha(this.registros.getString("fecha"));
//                    pro.setStatus(this.registros.getInt("status"));

                    // AGREGAR OBJETO A LA LISTA
                    listaProductos.add(pro);
                }

                this.desconectar();
            }
        } catch (SQLException e) {
            System.out.println("Error al encontrar registros: " + e.getMessage());
            throw e;
        }

        return listaProductos;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "SELECT * FROM productos WHERE codigo = ? and status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            registros = this.sqlConsulta.executeQuery();

            if (this.registros.next()) {
//                pro.setCodigo(codigo);
                pro.setIdproductos(this.registros.getInt("idproductos"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                this.desconectar();
            }

//            
        }

        return pro;
    }

}
