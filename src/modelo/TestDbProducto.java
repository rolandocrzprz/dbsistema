/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Rolando Cruz
 */
public class TestDbProducto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        dbProducto db = new dbProducto();

        if (db.conectar()) {
            System.out.println("Se conecto con exito");
        }

        // Insertar un nuevo producto para asegurarnos de que existe en la base de datos.
        /* Productos pro = new Productos();
        pro.setCodigo("1002");
        pro.setNombre("Camaron");
        pro.setFecha("2024-12-09");
        pro.setPrecio(30.50f);
        pro.setStatus(0);

        try {
            db.insertar(pro);
            JOptionPane.showMessageDialog(null, "Se agrego con exito");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Surgio un error: " + e.getMessage());
        }
         */
        // Actualizar el producto
        /* Productos proActualizado = new Productos();
        proActualizado.setCodigo("1002");
        proActualizado.setNombre("Camaron Azul");
        proActualizado.setFecha("2024-03-23");
        proActualizado.setPrecio(55.75f);
        proActualizado.setStatus(1);

        try {
            db.actualizar(proActualizado);
            JOptionPane.showMessageDialog(null, "Se actualizó con éxito");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Surgió un error al actualizar: " + e.getMessage());
        }
         */
        // HABILITAR
        /*  Productos proHabilitar = new Productos();
        proHabilitar.setCodigo("1002");

        try {
            db.habilitar(proHabilitar);
            JOptionPane.showMessageDialog(null, "Se habilito con éxito");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Surgió un error al actualizar: " + e.getMessage());
        }
         */
        // DESHABILITAR 
//         Productos proDeshabilitar = new Productos();
//        proDeshabilitar.setCodigo("9090");
//
//        try {
//            db.deshabilitar(proDeshabilitar);
//            JOptionPane.showMessageDialog(null, "Se deshabilito con éxito");
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Surgió un error al actualizar: " + e.getMessage());
//        }
         
        // VERIFICAR SI UN REGISTRO EXISTE EN NUESTA BASE DE DATOS 
//         try {
//            int id = 1; // ID del producto que queremos verificar
//            boolean existe = db.siExiste(id);
//            if (existe) {
//                JOptionPane.showMessageDialog(null, "El producto con ID " + id + " existe en la base de datos.");
//            } else {
//                JOptionPane.showMessageDialog(null, "El producto con ID " + id + " no existe en la base de datos.");
//            }
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Surgió un error al verificar la existencia del producto: " + e.getMessage());
//        }
//         
        // BUSCAR UN REGISTRO POR MEDIO DEL CODIGO DEL PRODUCTO
//         try {
//            // Buscar producto por código
//            String codigoBuscado = "1002";
//            Productos productoEncontrado = (Productos) db.buscar(codigoBuscado);
//
//            if (productoEncontrado != null) {
//                System.out.println("Producto encontrado:");
//                System.out.println("ID: " + productoEncontrado.getIdproductos());
//                System.out.println("Código: " + productoEncontrado.getCodigo());
//                System.out.println("Nombre: " + productoEncontrado.getNombre());
//                System.out.println("Precio: " + productoEncontrado.getPrecio());
//                System.out.println("Fecha: " + productoEncontrado.getFecha());
//                System.out.println("Status: " + productoEncontrado.getStatus());
//            } else {
//                System.out.println("No se encontró ningún producto con código " + codigoBuscado);
//            }
//        } catch (Exception e) {
//            System.out.println("Error al buscar producto: " + e.getMessage());
//        }
//            
//        
//        // LISTAR REGISTROS
        try {
            ArrayList<Productos> productos = db.lista();
            if (productos.isEmpty()) {
                System.out.println("No hay productos con status 0.");
            } else {
                for (Productos producto : productos) {
                    System.out.println("ID: " + producto.getIdproductos());
                    System.out.println("Código: " + producto.getCodigo());
                    System.out.println("Nombre: " + producto.getNombre());
                    System.out.println("Precio: " + producto.getPrecio());
                    System.out.println("Fecha: " + producto.getFecha());
//                    System.out.println("Status: " + producto.getStatus()) ;
                    System.out.println("----------------------------");
                }
            }
        } catch (Exception e) {
            System.out.println("Error al listar productos: " + e.getMessage());
        } finally {
            db.desconectar();
        }

    }

}
